<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

/**
 * Class Helper
 */
class Helper{

    const INPUT_DIR = __DIR__ . '/input';
    const OUTPUT_DIR = __DIR__ . '/output';

    /**
     * Get values of file
     *
     * @param string $filename
     *
     * @return array
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     */
    public static function getInputValues($filename){
        $reader = new PHPExcel_Reader_Excel2007();
        $book = $reader->load(self::INPUT_DIR . '/' . $filename);
        $sheet = $book->getSheet(0);

        $quantity_rows = $sheet->getHighestRow();
        $col_highest = $sheet->getHighestColumn();
        $values = [];

        for($row = 1; $row <= $quantity_rows; $row++){
            $key = self::_getHash($sheet, $row, $filename);
            $value = current($sheet->rangeToArray("A$row:$col_highest$row", null, true, true, true));

            // Formating and removing extra chars from cell with number
            if(mb_strpos($value[COL_NUMBER], ' от ') !== false){
                list($value[COL_NUMBER], $value[COL_DATE]) = self::_formatNumber($value[COL_NUMBER]);
            }

            // Invert name for app file
            if($filename === FILE_NAME_APP){
                $value[COL_NAME] = self::_formatName($value[COL_NAME]);
            }

            // Sum quantity for equal values
            if(isset($values[$key]) && self::_isEqual($value, $values[$key])){
                $values[$key][COL_QUANTITY] += $value[COL_QUANTITY];
            }else{
                $values[$key] = $value;
            }
        }

        return $values;
    }

    /**
     * Create sheets comparison
     *
     * @param PHPExcel $book
     * @param array $fact
     * @param array $app
     */
    public static function createCompare(PHPExcel $book, $fact, $app){
        $sheet_intersect = $book->getSheet(0);
        $sheet_intersect->setTitle('Intersection bw fact and app');

        $sheet_diff_fact_app = $book->createSheet();
        $sheet_diff_fact_app->setTitle('Diffs between fact and app');

        $sheet_diff_app_fact = $book->createSheet();
        $sheet_diff_app_fact->setTitle('Diffs between app and fact');

        $sheet_intersect_row = 1;
        $sheet_diff_fact_app_row = 1;
        $sheet_diff_app_fact_row = 1;

        foreach($fact as $key => $values){
            if(array_key_exists($key, $app)){
                foreach($values as $col_id => $col_value){
                    $sheet_intersect->setCellValue($col_id . $sheet_intersect_row, $col_value);
                }
                $sheet_intersect->setCellValue('F' . $sheet_intersect_row, $app[$key][COL_NAME]);
                $sheet_intersect->setCellValue('G' . $sheet_intersect_row, $app[$key][COL_QUANTITY]);
                $sheet_intersect_row++;
                continue;
            }
            self::_write($sheet_diff_fact_app, $sheet_diff_fact_app_row, $values);
        }

        foreach($app as $key => $values){
            if(array_key_exists($key, $fact)){
                continue;
            }
            self::_write($sheet_diff_app_fact, $sheet_diff_app_fact_row, $values);
        }
    }

    /**
     * @param PHPExcel $book
     * @param string $filename
     */
    public static function saveOutput(PHPExcel $book, $filename){
        $writer = new PHPExcel_Writer_Excel2007($book);
        $writer->save(self::OUTPUT_DIR . "/$filename.xlsx");
    }

    /**
     * Get hash by values of cols
     *
     * @param PHPExcel_Worksheet $sheet
     * @param int $row
     * @param string $filename
     *
     * @return string
     */
    private static function _getHash(PHPExcel_Worksheet $sheet, $row, $filename){
        $number = trim($sheet->getCell(COL_NUMBER . $row)->getValue());
        $title = trim($sheet->getCell(COL_TITLE . $row)->getValue());
        $name = trim($sheet->getCell(COL_NAME . $row)->getValue());
        if($filename === FILE_NAME_APP){
            // $name = self::_formatName($name);
        }
        if(mb_strpos($number, ' от ') !== false){
            $number = self::_formatNumber($number)[0];
        }

        return md5($number.$title);
    }

    /**
     * Write values to sheet
     *
     * @param PHPExcel_Worksheet $sheet
     * @param int $row
     * @param array $values
     */
    private static function _write(PHPExcel_Worksheet $sheet, &$row, $values){
        foreach($values as $col_id => $col_value){
            $sheet->setCellValue($col_id . $row, $col_value);
        }

        $row++;
    }

    /**
     * @param string $name
     * @return string
     */
    private static function _formatName($name){
        return trim(implode(' ', array_reverse(explode(' ', $name))));
    }

    /**
     * @param string $number
     * @return array
     */
    private static function _formatNumber($number){
        $result = explode(' от ', $number);
        $result[0] = trim(preg_replace('/[^0-9 .-]/i', '', $result[0]));

        return $result;
    }

    /**
     * @param array $one
     * @param array $two
     * @return bool
     */
    private static function _isEqual($one, $two){
        $cols = [COL_NUMBER, COL_DATE, COL_NAME, COL_TITLE];
        foreach($cols as $col){
            if($one[$col] != $two[$col]){
                return false;
            }
        }

        return true;
    }

}