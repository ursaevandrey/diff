<?php

require_once __DIR__ . '/helper.php';

$fact = Helper::getInputValues(FILE_NAME_FACT);
$app = Helper::getInputValues(FILE_NAME_APP);

$book = new PHPExcel();
Helper::createCompare($book, $fact, $app);
Helper::saveOutput($book, 'result2');